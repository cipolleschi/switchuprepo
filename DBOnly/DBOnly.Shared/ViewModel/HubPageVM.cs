﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DBOnly.ViewModel
{
    public class HubPageVM : BaseViewModel
    {
        public HubPageVM()
        {
            _students = new ObservableCollection<StudentVM>();
        }

        private ObservableCollection<StudentVM> _students;

        public ObservableCollection<StudentVM> Students
        {
            get { return _students; }
            set { _students = value; RaisePropertyChanged("Students"); }
        }

    }
}
