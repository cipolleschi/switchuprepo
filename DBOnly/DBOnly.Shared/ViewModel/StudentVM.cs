﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBOnly.ViewModel
{
    public class StudentVM : BaseViewModel
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; RaisePropertyChanged("Name"); }
        }

        private string _surname;

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; RaisePropertyChanged("Surname"); }
        }

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChanged("Id"); }
        }

        private DateTime _birthDate;

        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; RaisePropertyChanged("BirthDate"); }
        }

        private string _notes;

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; RaisePropertyChanged("Notes"); }
        }
        
    }
}
