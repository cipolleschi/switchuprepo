﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.WinRT.emptyApp.DataModel
{
    [Table("Courses")]
    public  class Course
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }

        public int TeacherId { get; set; }        
    }
}
