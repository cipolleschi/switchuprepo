﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.WP8.emptyApp.DataModel
{
    [Table("Students")]
    public sealed class Student
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string BirthDate { get; set; }
        
    }
}
