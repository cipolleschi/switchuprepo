﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.WP8.emptyApp.DataModel
{
    [Table("Courses")]
    public sealed class Course
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }

        public int TeacherId { get; set; }        
    }
}
