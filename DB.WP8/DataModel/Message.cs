﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.WP8.emptyApp.DataModel
{
    [Table("Messages")]
    public sealed class Message
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string Content { get; set; }
    }
}
