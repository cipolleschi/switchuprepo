﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.WP8.emptyApp.DataModel
{
    [Table("EnrolledStudents")]
    public sealed class EnrolledStudent
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int StudentId { get; set; }

        public int CourseId { get; set; }
    }
}
