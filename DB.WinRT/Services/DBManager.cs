﻿using DB.Shared;
using SQLite;
using SwitchUpKickStart.WinRT.emptyApp.DataModel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace SwitchUpKickStart.WinRT.emptyApp.Services
{
    /// <summary>
    /// Classe che gestisce le interazioni con il DB
    /// </summary>
    public class DBManager : IDBManager
    {
        /// <summary>
        /// Nome del database
        /// </summary>
        private static readonly string DBNAME = "Student.db";

        #region singleton
        private static readonly DBManager _instance = new DBManager();

        private DBManager() 
        {
                
        }

        /// <summary>
        /// Getter per accedere all'istanza SingleTon del DB
        /// </summary>
        public static DBManager Instance { 
            get { return _instance; } 
        }
        #endregion

        #region DBCreation
        private async Task<bool> DbExists(string dbName)
        {
            bool dbExists = true;
            try
            {
                StorageFile storageFile = await ApplicationData.Current.LocalFolder.GetFileAsync(dbName);                
            } 
            catch
            {
                dbExists = false;
            }

            return dbExists;
        }

        /// <summary>
        /// Metodo che crea il DB.
        /// </summary>
        /// <returns>Un task per concatenare le operazioni</returns>
        public async Task CreateDB()
        {
            SQLiteAsyncConnection conn = null;
            /*
            if ((await DbExists(DBNAME)))
            {
                conn = new SQLiteAsyncConnection(DBNAME);
                
                await conn.DropTableAsync<Teacher>();
                await conn.DropTableAsync<Course>();
                await conn.DropTableAsync<Student>();
                await conn.DropTableAsync<Message>();
                await conn.DropTableAsync<EnrolledStudent>();

            }
            */
            if(conn == null)
                conn = new SQLiteAsyncConnection(DBNAME);
            await conn.CreateTablesAsync<Teacher, Course, Student, Message, EnrolledStudent>();                
            
        }

        private SQLiteAsyncConnection openAsyncConnection()
        {
            return new SQLiteAsyncConnection(DBNAME);
        }
        #endregion

        /// <summary>
        /// Metodo che inserisce un oggetto nel DB
        /// </summary>
        /// <typeparam name="T">Il tipo dell'oggetto da inserire, deve essere una tabella</typeparam>
        /// <param name="entity">L'oggetto da inserire nel db</param>
        /// <returns>Un task per concatenare le operazioni</returns>
        public async Task InsertEntity<T>(T entity)
        {
            await openAsyncConnection().InsertAsync(entity);            
        }

        /// <summary>
        /// Metodo che rimuove un'entità dal db
        /// </summary>
        /// <typeparam name="T">Il tipo dell'entità da rimuovere</typeparam>
        /// <param name="entity">L'entità da rimuovere</param>
        /// <returns>Un task per concatenare le operazioni</returns>
        public async Task DeleteEntity<T>(T entity)
        {
            await openAsyncConnection().DeleteAsync(entity);
        }

        /// <summary>
        /// Metodo che aggiorna un'entità nel DB
        /// </summary>
        /// <typeparam name="T">Il tipo dell'entità da aggiornare</typeparam>
        /// <param name="entity">L'entità da aggiornare</param>
        /// <returns>Un task per concatenare le operazioni</returns>
        public async Task UpdateEntity<T>(T entity)
        {
            await openAsyncConnection().UpdateAsync(entity);
        }

        /// <summary>
        /// Metodo che recupera un'entità dal db, a partire dalla sua chiave
        /// </summary>
        /// <typeparam name="TEntity">Il tipo dell'entità da recuperare</typeparam>
        /// <param name="key">La chiave dell'entità</param>
        /// <returns>L'entità recuperata dall'operazione di get</returns>
        public async Task<TEntity> Get<TEntity>(object key) where TEntity : new()
        {
            return await openAsyncConnection().GetAsync<TEntity>(key);
        }

        /// <summary>
        /// Metodo che recupera un entità dal db, con una funzione di selezione custom
        /// </summary>
        /// <typeparam name="TEntity">Il tipo di entità da recuperare</typeparam>
        /// <param name="predicate">La funzione custom</param>
        /// <returns>L'entità recuperata dall'operazione di GEt</returns>
        public async Task<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : new()
        {
            return await openAsyncConnection().GetAsync<TEntity>(predicate);
        }
    }
}
