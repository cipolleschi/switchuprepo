﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.WinRT.emptyApp.DataModel
{
    [Table("Teachers")]
    public sealed class Teacher
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public double Salary { get; set; }
    }
}
