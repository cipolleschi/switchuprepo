﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DB.Shared
{
    public interface IDBManager
    {
        Task CreateDB();

        Task InsertEntity<T>(T entity);

        Task UpdateEntity<T>(T entity);

        Task DeleteEntity<T>(T entity);

        Task<TEntity> Get<TEntity>(object key) where TEntity : new();

        Task<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : new();
    }
}
