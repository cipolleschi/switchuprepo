﻿using SwitchUpKickStart.WinRT.emptyApp.DataModel;
using SwitchUpKickStart.WinRT.emptyApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.ApplicationModel.Background;

namespace BGTask.WinRT
{
    public sealed class BGTask : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            await DBManager.Instance.InsertEntity<Message>(new Message
            {
                Timestamp = DateTime.Now,
                Content = String.Format("Message content: {0}", Guid.NewGuid())
            });
        }
    }
}
