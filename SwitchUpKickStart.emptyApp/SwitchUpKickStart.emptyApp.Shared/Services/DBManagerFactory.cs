﻿using DB.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwitchUpKickStart.emptyApp.Services
{
    public class DBManagerFactory
    {
        public static IDBManager DBManager
        {
            get
            {
#if WINDOWS_PHONE_APP
                return SwitchUpKickStart.WP8.emptyApp.Services.DBManager.Instance;
#else
                return SwitchUpKickStart.WinRT.emptyApp.Services.DBManager.Instance;
#endif

            }
        }
    }
}
